#!/bin/bash
#    mcc: Mabox Control Center
#    Copyright (C) 2019-2025 napcok <napcok@gmail.com>
#
if hash btop 2>/dev/null; then
        BTOP="btop"
    else
        BTOP="bpytop"
fi
if hash fastfetch 2>/dev/null; then
        FETCH="fastfetch"
    else
        FETCH="neofetch"
fi

case $LANG in
    pl*)
        TITLE="Centrum Sterowania Mabox"
        MCC="<big><b>Centrum Sterowania Mabox</b></big>\t\n<i>Skonfiguruj wygląd i zachowanie swojego Maboxa</i>\t"
        START="Start"
        SYSTEM="System i Sprzęt"
        LOCALE_SETTINGS="Język i formaty"
        LANGUAGE_PACKAGES="Pakiety językowe"
        KERNEL="Jądro systemowe"
        USER_ACCOUNTS="Konta użytkowników"
        TIME_DATE="Data i Czas"
        KEYBOARD="Mysz i Klawiatura"
        HARDWARE="Konfiguracja sprzętowa"
        SOFTWARE="Programy"
        AUTOSTART="Autostart"
        AUTOSTART_HEAD="Autostart"
        LOOK="Wygląd"
        TINT2="Panel Tint2"
        SETTINGS="Ustawienia"
        CONKY="Conky"
        MENU="Menu/Panele boczne"
        THEMES="Motywy"
        HELP="Uzyskaj Pomoc"
        HELP_TXT="Visit <a href='https://manual.maboxlinux.org/en/'>Mabox Linux Manual</a> to learn more about Mabox.    \nDo you have questions? Want to get involved?\nTake a look at our: <a href='https://maboxlinux.org/'>official website</a>, <a href='https://forum.maboxlinux.org/'>forum</a>, <a href='https://blog.maboxlinux.org/'>blog</a>\n                                                                              \n<i>Mabox Linux is developed with passion in spare time.\nMabox is free and it will always be.\nIf you like Mabox you can help by making a small <a href='https://ko-fi.com/maboxlinux'>donation</a></i>         \nThank you for choosing to use Mabox Linux!\n"
        SYSTEM_DESC="Ustawienia oraz informacje systemowe i sprzętowe"
        MONITORS="<b>Monitor(y)</b>"
        SOFTWARE_DESC="Aktualizacja - Instalacja Programów - Preferowane programy\n"
        AUTOSTART_DESC="<a href='https://manual.maboxlinux.org/en/configuration/autostart/'>Pomoc (online)</a>\nOpenbox używa dwóch mechanizmów autostartu.\nPierwszy z nich to autostart XDG."
        AUTOSTART_DESC2="Drugim - specyficznym dla Openbox - jest skrypt <i><b>~/.config/openbox/autostart</b></i>."
        EDIT_XDG="Wybierz programy autostartu XDG"
        EDIT_SCRIPT="Edytuj skrypt"
        AUTOSTART_RESET="Przywróć domyślny skrypt autostartu"
        LOOK_DESC="Narzędzia do konfiguracji wyglądu\n"
        EDIT_FILE="Edytuj plik"
        TINT_DESC="<b>Konfigurator paneli tint2</b>\nTutaj możesz wybrać konfigurację panelu Tint2.\nW Maboxie dostępne są różne konfiguracje panelu tint2, możesz również dodać własne do katalogu <i>~/.config/tint2</i>."
        T_CONF="Konfiguruj panel"
        T_CHOOSE="Wybierz tint2!!Możesz uruchomić kilka paneli"
        T_RESTART="Restartuj panel(e)"
        T_LAUNCHERS="Dodaj/usuń programy do panelu"
        TINT_DIR="Otwórz katalog <i>~/.config/tint2/</i> w menadżerze plików"
        CONKY_DESC="<b>Menedżer Conky</b><i>(beta)</i>\nWybierz uruchamiane Conky. W Maboxie dostępnych jest kilka konfiguracji Conky, możesz dodać własne do katalogu <i>~/.config/conky</i>."
        CONKY_RESTART="Restartuj Conky"
        OPEN_CONKYDIR="Otwórz katalog <i>~/.config/conky</i>"
        MENU_DESC="Główne menu (<small>wywoływane przez: prawy klik, skrót win+spacja lub z ikony w panelu</small>) umożliwia wyszukiwanie, należy po prostu zacząć pisać np. nazwę programu.\n<small><i>Dostępne są również dwa <b>panele boczne</b>, umożliwiające szybki dostęp np. do systemu plików.</i>\nDo menu oraz paneli możesz dodawać swoje własne polecenia - <a href='https://blog.maboxlinux.org/how-to-add-custom-commands-to-menu-and-sidepanels/'>post na devblogu</a></small>"
        M_EDIT_MAIN="Ulubione"
        M_EDIT_MAIN_AFTER="Pod Aplikacjami"
        M_RESTORE_MAIN="Przywróć domyślne menu"
        M_LEFT="<b>PANEL LEWY</b>"
        M_LEFT_KEY="<small>(ctrl+super+left)</small>"
        M_MAIN="<b>MENU GŁÓWNE</b>"
        M_MAIN_KEY="<small>(super)</small>"
        SETTINGS_MENU="<b>MENU USTAWIEŃ</b>"
        SETTINGS_EDIT="Edytuj..."
        M_RIGHT="<b>PANEL PRAWY</b>"
        M_RIGHT_KEY="<small>(ctrl+super+left)</small>"
        M_LEFT_DESC="<small>Lewy panel\n - szybka nawigacja po systemie plików\n - zakładki GTK\n - maszyny wirtualne</small>"
        M_RIGHT_DESC="<small>Prawy panel\n - ustawienia systemowe\n - pomoc\n - wyjście</small>"
        M_CUSTOMIZE="<i>Edytuj komendy:</i>"
        M_MORE="<b>WIĘCEJ USTAWIEŃ...</b>"
        M_COLORSETTINGS="<b>Kolorystyka...</b>"
        OWN_COMMANDS_TOP="(Góra)"
        OWN_COMMANDS_BOTTOM="(Dół)"
        COMPOSITOR="Kompozytor"
        COMP_DESC="Menadżerem kompozycji w Maboxie jest <b>Picom</b> - fork Compton.\nSkrót klawiszowy do włączania/wyłączania menadżera kompozycji to (<b>win</b>+<b>p</b>).\n\nMabox dostarcza kilku plików konfiguracyjnych dla Picoma.\nMożesz je łatwo przełączać za pomocą Manadżera Picom."
        COMP_GUI="Ustawienia"
        COMP_EDIT="Menadżer Picom"
        COMP_RESTART="Restart"
        COMP_TOGGLE="Włącz/Wyłącz (<b>win</b>+<b>p</b>)"
        COMP_DIR="Otwórz katalog z konfiguracjami Picom"
        MT_MNGR="Menedżer Motywów Maboxa"
        MT_MNGR_DESC="\n<a href='https://manual.maboxlinux.org/en/configuration/theme-manager/'>Pomoc (online)</a>\nMotyw Maboxa składa się z:\n - tapety\n - wystroju GTK2/GTK3 oraz obramowania okien Openboxa\n - ustawień panelu Tint2\n - uruchamianych automatycznie Conky                              \n\nZa pomocą menedżera motywów możesz w wygodny sposób zapisywać swoje konfiguracje, a następnie dowolnie przełączać się między nimi."
        COLORIZER_DESC="<b>Colorizer</b>\nUmożliwia zmianę kolorystyki elementów pulpitu Mabox.\nMoże również automatycznie generować kolorystykę na bazie tapety.\n<i>Zobacz video (online):</i> <a href='https://youtu.be/h50oeS7aFyM'>Colorizer - krótki przegląd</a>\n"
        COL_MODULES="Moduły Główne"
        COL_ADDONS="<i>dodatki:</i>"
        COL_MENU="Colorizer RootMenu"
        LNG="pl"
        USER_LBL="użytkownik"
        USERS_MSM="Użytkownicy"
        RES="rozdzielczość"
        PKGS_INSTALLED="zainstalowane pakiety"
        PKGS_STAT="Statystyki"
        UPDCHECK="sprawdź aktualizacje"
        CLI_UPD="Aktualizacja CLI"
        KERNEL_LBL="jądro"
        KERNELS="Jądra"
        INSTALLED="zainstalowany:"
        DAYS_AGO="dni temu"
        MABOX_DESC="Twój lekki, szybki i funkcjonalny Linuksowy Desktop"
        ADVANCED="Zaawansowane"
        FONTS="Czcionki"
        FONT_DESC="<b>Czcionki</b>\nUżuj Menu Konfiguracji Czcionek aby ustawić czcionki dla:\n - Openbox - tytuł okna\n - Menu i Panele Boczne\n - czcionka GTK\n - Conky"
        FONT_MENU="Menu Konfiguracji Czcionek"
        FONT_INC="Powiększ Wszystkie"
        FONT_DEC="Pomniejsz Wszystkie"
        FONT_RESET="Resetyj Wszystkie"
        WALLPAPER="Tapety"
        WALLPAPER_DESC="<b>Jak ustawić tapetę w Maboxie?</b>\n\nCzy widzisz ikonę z obrazkiem w panelu?\nKliknij w nią prawym przyciskiem myszy...\n"
        WP_RANDOM="Losowa tapeta"
        WP_CHOOSE="Wybierz w menadżerze plików"
        WP_PREVIEW="Przeglądaj i wybierz"
        WP_SLIDE="Pokaz slajdów"
        WP_GENERATE="Generator tapet"
        WP_DIRS="Edytuj katalogi z tapetami"
        WP_TITLE="Jak ustawić tapetę w Maboxie?"
        WP_MSG="\nKlikając prawym przyciskiem myszy w ikonę w panelu masz kilka sposobów ustawienia tapety:\n- <b>losowa</b>,\n- <b>wybierz</b> <i>(z menu kontekstowego w menadżerze plików)</i>,\n- <b>przeglądaj i wybierz</b>,\n- <b>pokaz slajdów</b>\n- lub <b>generator</b> tapet\n\nNa dole menu możesz ustawić akcję dla lewego kliknięcia w ikonę.\n\nWypróbuj różne opcje i ustaw tą, która najbardziej Ci pasuje."
        
        ;;
    es*)
        TITLE="Centro de control Mabox"
        MCC="<big><b>Centro de control Mabox</b></big>\t\nConfigura y ajusta tu sistema Mabox\t"
        START="Start"
        SYSTEM="Sistema/Hardware"
        LOCALE_SETTINGS="Configuración regional"
        LANGUAGE_PACKAGES="Paquetes de idiomas"
        KERNEL="Núcleo"
        USER_ACCOUNTS="Cuentas de usuario"
        TIME_DATE="Hora y fecha"
        KEYBOARD="Mouse e teclado"
        HARDWARE="Configuración de hardware"
        SOFTWARE="Programas"
        AUTOSTART="Programas de inicio"
        AUTOSTART_HEAD="Programas de inicio"
        LOOK="Apariencia"
        TINT2="Panel Tint2"
        SETTINGS="Ajustes"
        CONKY="Recuadro Conky"
        MENU="Menú y Paneles"
        THEMES="Temas"
        HELP="Get Help"
        HELP_TXT="Visit <a href='https://manual.maboxlinux.org/es/'>Mabox Linux Manual</a> to learn more about Mabox.    \nDo you have questions? Want to get involved?\nTake a look at our: <a href='https://maboxlinux.org/'>official website</a>, <a href='https://forum.maboxlinux.org/'>forum</a>, <a href='https://blog.maboxlinux.org/'>blog</a>\n                                                                              \n<i>Mabox Linux is developed with passion in spare time.\nMabox is free and it will always be.\nIf you like Mabox you can help by making a small <a href='https://ko-fi.com/maboxlinux'>donation</a></i>         \nThank you for choosing to use Mabox Linux!\n"
        SYSTEM_DESC="Ajustes e información del sistema y de hardware"
        MONITORS="<b>Monitor(es)</b>"
        SOFTWARE_DESC="Instalación y actualización de programas - Aplicaciones preferidas.\n"
        AUTOSTART_DESC="<a href='https://manual.maboxlinux.org/es/configuration/autostart/'>Info (online)</a>\nOpenbox ocupa 2 métodos de reinicio.\nEl primero es reinicio por XDG."
        AUTOSTART_DESC2="El segundo método de reinicio es usar el mismo archivo script de Openbox:\n<i><b>~/.config/openbox/autostart</b></i>. "
        EDIT_XDG="Seleccionar ítemes para reinicio"
        EDIT_SCRIPT="Editar el archivo script de reinicio"
        AUTOSTART_RESET="Reestablecer el archivo script de reinicio por defecto"
        LOOK_DESC="Configurar la apariencia de tu escritorio.\n"
        EDIT_FILE="Editar el archivo"
        TINT_DESC="<b>Configurador de paneles Tint2</b>\nAquí puede elegir los ajustes para los paneles.\nExisten varios ajustes predefinidos en Mabox, y además puede agregar nuevos en este directorio <i>~/.config/tint2</i> ."
        T_CONF="Configurar panel"
        T_CHOOSE="Elija un panel tint2!!Puede ejecutar varios paneles de inmediato"
        T_RESTART="Reiniciar panel tint2"
        T_LAUNCHERS="Add/remove launchers..."
        TINT_DIR="Abrir la carpeta <i>~/.config/tint2/</i> "
        CONKY_DESC="<b>Conky (on steroids)</b>\nConky in Mabox has been equipped with additional powers not available in any other Linux distribution!\n\nRight click on any Conky to see handy context menu... <i>try it now!</i>\n\nYou can also define your own left-click command (or a multi-command menu)."
        CONKY_RESTART="Recargar recuadro Conky"
        OPEN_CONKYDIR="Abrir la carpeta <i>~/.config/conky</i> "
        MENU_DESC="Menu principal (<small>accede con clic derecho, teclas Super+barra de espacio o desde el ícono del panel</small>) activa escribe sobre buscar. Sólo empieza a escribir el término que estas buscando.\n<small><i>There are also two <b>side-panels</b> available for quick access to file system locations for example.</i>You can add custom commands to menus and side-panels - <a href='https://blog.maboxlinux.org/how-to-add-custom-commands-to-menu-and-sidepanels/'>blogpost</a></small>"
        M_EDIT_MAIN="Favoritos"
        M_EDIT_MAIN_AFTER="Below Apps"
        M_RESTORE_MAIN="Reestablecer el menu por defecto"
        M_LEFT="<b>PANEL IZQUIERDO</b>"
        M_LEFT_KEY="<small>(ctrl+super+left)</small>"
        M_MAIN="<b>MENU PRINCIPAL</b>"
        M_MAIN_KEY="<small>(super)</small>"
        SETTINGS_MENU="<b>SETTINGS MENU</b>"
        SETTINGS_EDIT="Editar..."
        M_RIGHT="<b>PANEL DERECHO</b>"
        M_RIGHT_KEY="<small>(ctrl+super+right)</small>"
        M_LEFT_DESC="<small>Panel izquierdo\n - Navegacion veloz\n - Marcadores GTK\n - Máquinas Virtuales</small>"
        M_RIGHT_DESC="<small>Panel derecho\n - Configuración del sistema\n - ayuda\n - opciones de salida</small>"
        M_CUSTOMIZE="<i>Ajustes:</i>"
        M_MORE="<b>MORE SETTINGS..</b>"
        M_COLORSETTINGS="<b>Coloring...</b>"
        OWN_COMMANDS_TOP="(TOP)"
        OWN_COMMANDS_BOTTOM="(BOTTOM)"
        COMPOSITOR="Compositor gráfico"
        COMP_DESC="<b>Picom</b> is used as composite manager in Mabox.\n\nWith the Picom Manager you can choose which of the several available configurations you want to use. You can also add your own.\n\n"
        COMP_GUI="Configuración"
        COMP_EDIT="Picom Manager"
        COMP_RESTART="Reiniciar compositor gráfico"
        COMP_TOGGLE="Activar/desactivar compositor (<b>win</b>+<b>p</b>)"
        COMP_DIR="Open Picom config directory"
        MT_MNGR="Gestor de Temas Mabox"
        MT_MNGR_DESC="\n<a href='https://manual.maboxlinux.org/es/configuration/theme-manager/'>Help (online)</a>\nTemas Mabox consiste de:\n - fondos de pantalla\n - temas GTK2/GTK3\n - ajustes a tema Openbox\n - paneles Tint2 seleccionados\n - recuadros Conky seleccionados\n\nCon el gestor de Temas en Mabox puede fácilmente guardar nuevas configuraciones, y cambiar entre ellas."
        COLORIZER_DESC="<b>Colorizer</b>\nColorizer allows you to change the color of the Mabox desktop elements.\nIt can also generate themes based on the wallpaper colors or monochrome themes from color picked from screen.\n"
        COL_MODULES="Core Modules:"
        COL_ADDONS="<i>addons:</i>"
        COL_MENU="Colorizer RootMenu"
        LNG="en"
        USER_LBL="user"
        USERS_MSM="Users"
        RES="resolution"
        PKGS_INSTALLED="installed pkgs"
        PKGS_STAT="Statistics"
        UPDCHECK="check updates"
        CLI_UPD="CLI update"
        KERNEL_LBL="kernel"
        KERNELS="Kernels"
        INSTALLED="install date: "
        DAYS_AGO="days ago"
        MABOX_DESC="Your fast, lightweight and functional Linux Desktop"
        ADVANCED="Advanced"
        FONTS="Fonts"
        FONT_DESC="<b>Fonts</b>\nUse Font Config Menu to set fonts for:\n - Openbox window title\n - Menus and Side panels\n - GTK font\n - Conkies"
        FONT_MENU="Font Config Menu"
        FONT_INC="Increase All"
        FONT_DEC="Decrease All"
        FONT_RESET="Reset All"
        WALLPAPER="Wallpaper"
        WALLPAPER_DESC="<b>How to set Wallpaper in Mabox?</b>\n\nDo you see an icon with a picture in the tint2 panel?\nRight-click on it...\n"
        WP_RANDOM="Random wallpaper"
        WP_CHOOSE="Choose from file manager"
        WP_PREVIEW="Preview &amp; choose"
        WP_SLIDE="Slideshow"
        WP_GENERATE="Generate"
        WP_DIRS="Edit Wallpapers Dirs"
        WP_TITLE="How to set Wallpaper in Mabox?"
        WP_MSG="\nFrom right-click on icon in panel you have access to several ways to set the wallpaper:\n- <b>random</b>,\n- <b>choose</b> <i>(using context menu in filemanager)</i>,\n- <b>preview and set</b>,\n- <b>slideshow</b>\n- or <b>generate</b>\n\nAt the bottom of the menu you have the option to assign the desired action to the left-click on the icon on the panel.\n\nTest different options and assign the one that suits you best."
        
        ;;
    *)
        TITLE="Mabox Control Center"
        MCC="<big><b>Mabox Control Center</b></big>\t\n<i>Configure and customize your Mabox Linux</i>\t"
        START="Start"
        SYSTEM="System/HW"
        LOCALE_SETTINGS="Locale Settings"
        LANGUAGE_PACKAGES="Language Packages"
        KERNEL="Kernel"
        USER_ACCOUNTS="User Accounts"
        TIME_DATE="Time and Date"
        KEYBOARD="Mouse and Keyboard Settings"
        HARDWARE="Hardware Configuration"
        SOFTWARE="Software"
        AUTOSTART="Autostart"
        AUTOSTART_HEAD="Autostart"
        LOOK="Look and Feel"
        TINT2="Tint2 Panel"
        SETTINGS="Settings"
        CONKY="Conky"
        MENU="Menu/SidePanels"
        THEMES="Themes"
        HELP="Get Help"
        HELP_TXT="Visit <a href='https://manual.maboxlinux.org/en/'>Mabox Linux Manual</a> to learn more about Mabox.    \nDo you have questions? Want to get involved?\nTake a look at our: <a href='https://maboxlinux.org/'>official website</a>, <a href='https://forum.maboxlinux.org/'>forum</a>, <a href='https://blog.maboxlinux.org/'>blog</a>\n                                                                              \n<i>Mabox Linux is developed with passion in spare time.\nMabox is free and it will always be.\nIf you like Mabox you can help by making a small <a href='https://ko-fi.com/maboxlinux'>donation</a></i>         \nThank you for choosing to use Mabox Linux!\n"
        SYSTEM_DESC="System and Hardware settings and information"
        MONITORS="<b>Monitor(s)</b>"
        SOFTWARE_DESC="Software installation and update - Preferred Applications.\n"
        AUTOSTART_DESC="<a href='https://manual.maboxlinux.org/en/configuration/autostart/'>Info (online)</a>\nOpenbox uses two autostart methods.\nFirst is XDG autostart."
        AUTOSTART_DESC2="Second method is Openbox own autostart script: <i><b>~/.config/openbox/autostart</b></i>. "
        EDIT_XDG="Select items to autostart"
        EDIT_SCRIPT="Edit script"
        AUTOSTART_RESET="Reset to default autostart script"
        LOOK_DESC="Customize Look and Feel of your desktop.\n"
        EDIT_FILE="Edit file"
        TINT_DESC="<b>Tint2 panels Configurator</b>\nHere you can choose Tint2 panel(s) configuration.\nThere are some predefined configurations in Mabox, you can also add your own to <i>~/.config/tint2</i> directory."
        T_CONF="Configure panel"
        T_CHOOSE="Choose tint2!!You can run several tint2 panels at once"
        T_RESTART="Restart tint2"
        T_LAUNCHERS="Add/remove launchers..."
        TINT_DIR="Open <i>~/.config/tint2/</i> directory "
        CONKY_DESC="<b>Conky (on steroids)</b>\nConky in Mabox has been equipped with additional powers not available in any other Linux distribution!\n\nRight click on any Conky to see handy context menu... <i>try it now!</i>\n\nYou can also define your own left-click command (or a multi-command menu)."
        CONKY_RESTART="Reload Conky"
        OPEN_CONKYDIR="Open <i>~/.config/conky</i> directory"
        MENU_DESC="Main menu (<small>access it by right click, win+space shortcut or from panel icon</small>) have type to search functionality. Just start to type what you looking for.\n<small><i>There are also two <b>side-panels</b> available for quick access to file system locations for example.</i> You can add custom commands to menus and side-panels <a href='https://blog.maboxlinux.org/how-to-add-custom-commands-to-menu-and-sidepanels/'>blogpost</a></small>"
        M_EDIT_MAIN="Edit Favorites"
        M_EDIT_MAIN_AFTER="Edit below Apps"
        M_RESTORE_MAIN="Reset mainmenu to default"
        M_LEFT="<b>LEFT PANEL</b>"
        M_LEFT_KEY="<small>(ctrl+super+left)</small>"
        M_MAIN="<b>MAIN MENU</b>"
        M_MAIN_KEY="<small>(super)</small>"
        SETTINGS_MENU="<b>SETTINGS MENU</b>"
        SETTINGS_EDIT="Edit..."
        M_RIGHT="<b>RIGHT PANEL</b>"
        M_RIGHT_KEY="<small>(ctrl+super+right)</small>"
        M_LEFT_DESC="<small>Left panel\n - quick navigation\n - GTK Bookmarks\n - Virtualbox machines</small>"
        M_RIGHT_DESC="<small>Right panel\n - system settings\n - help\n - exit options</small>"
        M_CUSTOMIZE="<i>Customize commands:</i>"
        M_MORE="<b>MORE SETTINGS..</b>"
        M_COLORSETTINGS="<b>Coloring...</b>"
        OWN_COMMANDS_TOP="(TOP)"
        OWN_COMMANDS_BOTTOM="(BOTTOM)"
        COMPOSITOR="Compositor"
        COMP_DESC="<b>Picom</b> is used as composite manager in Mabox.\n\nWith the Picom Manager you can choose which of the several available configurations you want to use. You can also add your own.\n\n"
        COMP_EDIT="Picom Manager"
        COMP_RESTART="Restart"
        COMP_TOGGLE="Toggle (<b>win</b>+<b>p</b>)"
        COMP_DIR="Open Picom config directory"
        MT_MNGR="Mabox Theme Manager"
        MT_MNGR_DESC="\n<a href='https://manual.maboxlinux.org/en/configuration/theme-manager/'>Help (online)</a>\nMabox theme consist of:\n - wallpaper\n - GTK2/GTK3 Theme and Openbox window decoration          \n - selected Tint2 panel(s)\n - selected Conkies                                \n\nWith Mabox Theme Manager you can easily save your configurations, and switch between them."
        COLORIZER_DESC="<b>Colorizer</b>\nColorizer allows you to change the color of the Mabox desktop elements.\nIt can also generate themes based on the wallpaper colors or monochrome themes from color picked from screen.\n"
        COL_MODULES="Core Modules:"
        COL_ADDONS="<i>addons:</i>"
        COL_MENU="Colorizer RootMenu"
        LNG="en"
        USER_LBL="user"
        USERS_MSM="Users"
        RES="resolution"
        PKGS_INSTALLED="installed pkgs"
        PKGS_STAT="Statistics"
        UPDCHECK="check updates"
        CLI_UPD="CLI update"
        KERNEL_LBL="kernel"
        KERNELS="Kernels"
        INSTALLED="install date: "
        DAYS_AGO="days ago"
        MABOX_DESC="Your fast, lightweight and functional Linux Desktop"
        ADVANCED="Advanced"
        FONTS="Fonts"
        FONT_DESC="<b>Fonts</b>\nUse Font Config Menu to set fonts for:\n - Openbox window title\n - Menus and Side panels\n - GTK font\n - Conkies"
        FONT_MENU="Font Config Menu"
        FONT_INC="Increase All"
        FONT_DEC="Decrease All"
        FONT_RESET="Reset All"
        WALLPAPER="Wallpaper"
        WALLPAPER_DESC="<b>How to set Wallpaper in Mabox?</b>\n\nDo you see an icon with a picture in the tint2 panel?\nRight-click on it...\n"
        WP_RANDOM="Random wallpaper"
        WP_CHOOSE="Choose from file manager"
        WP_PREVIEW="Preview &amp; choose"
        WP_SLIDE="Slideshow"
        WP_GENERATE="Generate"
        WP_DIRS="Edit Wallpapers Dirs"
        WP_TITLE="How to set Wallpaper in Mabox?"
        WP_MSG="\nFrom right-click on icon in panel you have access to several ways to set the wallpaper:\n- <b>random</b>,\n- <b>choose</b> <i>(using context menu in filemanager)</i>,\n- <b>preview and set</b>,\n- <b>slideshow</b>\n- or <b>generate</b>\n\nAt the bottom of the menu you have the option to assign the desired action to the left-click on the icon on the panel.\n\nTest different options and assign the one that suits you best."
        ;;
esac

maindialog () {
	KEY=$RANDOM

	res1=$(mktemp --tmpdir mcc-tab1.XXXXXXXX)
	res2=$(mktemp --tmpdir mcc-tab2.XXXXXXXX)
	res3=$(mktemp --tmpdir mcc-tab3.XXXXXXXX)
	res4=$(mktemp --tmpdir mcc-tab4.XXXXXXXX)
	res5=$(mktemp --tmpdir mcc-tab5.XXXXXXXX)
	res6=$(mktemp --tmpdir mcc-tab6.XXXXXXXX)
	res7=$(mktemp --tmpdir mcc-tab7.XXXXXXXX)
	res8=$(mktemp --tmpdir mcc-tab8.XXXXXXXX)
	res9=$(mktemp --tmpdir mcc-tab9.XXXXXXXX)
    res10=$(mktemp --tmpdir mcc-tab10.XXXXXXXX)
    res11=$(mktemp --tmpdir mcc-tab11.XXXXXXXX)
    res12=$(mktemp --tmpdir mcc-tab12.XXXXXXXX)
    res13=$(mktemp --tmpdir mcc-tab13.XXXXXXXX)
    out=$(mktemp --tmpdir mcc-out.XXXXXXXX)

# cleanup
trap "rm -f $res1 $res2 $res3 $res4 $res5 $res6 $res7 $res8 $res9 $res10 $res11 $res12 $res13 $out" EXIT

OSNAME=$(lsb_release -d | awk '{print $2}')
OSVERSION=$(lsb_release -r | awk '{print $2}')
#OSCODE=$(lsb_release -c | awk '{print $2}')
OSCODE=$(grep CODENAME /etc/lsb-release |cut -d '=' -f2)

 # system uptime
UPT="$(uptime -p)"
UPT="${UPT/up /}"
UPT="${UPT/ day?/d}"
UPT="${UPT/ hour?/h}"
UPT="${UPT/ minute?/m}"
# install date
if [[ -e /var/log/pacman.log ]]; then
        INST="$(sed 1q /var/log/pacman.log)"
        INST="${INST/ */}"
        INST="${INST/T*/}" # newer pacman adds a different timestamp (TXX:XX:XX+0000), we just want the date
        INST="${INST/\[/}"
        #INST="${INST//\-/ }"
fi
# Days ago
TODAY=$(date +%F)
let DIFF=($(date +%s -d ${TODAY})-$(date +%s -d ${INST}))/86400
if [[ "$DIFF" > "1" ]];then
DAGO="(${DIFF} ${DAYS_AGO})"
else
DAGO=""
fi

PKGS=$(pacman -Qq 2>/dev/null | wc -l)
# kernel version
KERN="${KERN:-$(uname -sr | awk '{print $2}')}"
KERN="${KERN/-*/}"
KERN="${KERN/linux/}"
KERN="${KERN,,}"

RESOLUTION=$(xdpyinfo | awk '/^ +dimensions/ {print $2}')

INTRO_TXT=$(cat <<EOF
\t<big>Mabox Linux $OSVERSION</big> <i>$OSCODE</i>
\t<small><i>$MABOX_DESC</i></small>\n
\t<small><tt>$INSTALLED </tt></small><b>$INST</b> <small>$DAGO</small>
\t<small><tt>wm:            </tt></small><b>$XDG_SESSION_DESKTOP</b>
\t<small><tt>uptime:        </tt></small><b>$UPT</b>
\n
EOF
)
#INTRO
GDK_BACKEND=x11 yad --plug=$KEY --tabnum=1 --borders=10 \
--text="$INTRO_TXT" --image=/usr/share/icons/hicolor/128x128/apps/mbcc.png \
--columns=4 --form \
--field="$USER_LBL:LBL" "" \
--field="$PKGS_INSTALLED:LBL" "" \
--field="$UPDCHECK:LBL" "" \
--field="$KERNEL_LBL:LBL" "" \
--field="$RES:LBL" "" \
--field="<b>$USER</b>:LBL" "" \
--field="<b>$PKGS</b>:LBL" "" \
--field="<b>>>></b>:LBL" "" \
--field="<b>$KERN</b>:LBL" "" \
--field="<b>$RESOLUTION</b>:LBL" "" \
--field=" $USERS_MSM!system-config-users!:FBTN" "manjaro-settings-manager -m msm_users" \
--field=" Pamac!package-manager-icon!:FBTN" "pamac-manager" \
--field=" Pamac updater!software-update!:FBTN" "pamac-manager --updates" \
--field=" $KERNELS!distributor-logo-linux!:FBTN" "manjaro-settings-manager -m msm_kernel" \
--field=" ARandr!system-terminal!:FBTN" "arandr" \
--field=" :LBL" "" \
--field=" $PKGS_STAT (<small><tt>yay -Ps</tt></small>)!utilities-terminal!:FBTN" "terminator -T 'Package stats' -e 'yay -Ps;read'" \
--field=" $CLI_UPD (<small><tt>yay</tt></small>)!utilities-terminal!:FBTN" "terminator -e 'bash -c yay;read'" \
--field=" :LBL" "" \
--field=" LXRandr!system-terminal!:FBTN" "lxrandr" \
> $res1 &


# TINT2
GDK_BACKEND=x11 yad --plug=$KEY --tabnum=2 --borders=20 \
--text="$TINT_DESC \n" --image=tint2conf \
--columns=1 --form \
--field="$T_LAUNCHERS:FBTN" "jgtint2launcher" \
--field="$T_CONF:FBTN" "jgtint2-pipe -s" \
--field="$T_RESTART:FBTN" "mb-tint2restart" \
--field="$ADVANCED:LBL" "" \
--field="$T_CHOOSE:FBTN" "mb-tint2-manager" \
--field="$TINT_DIR:FBTN" "exo-open $HOME/.config/tint2/" \
> $res2 &

# MENU
GDK_BACKEND=x11 yad --plug=$KEY --tabnum=3 --borders=1 \
--text="$MENU_DESC" --image=menu-editor \
--columns=4 --form \
--field="$M_LEFT:FBTN" "mb-jgtools places" \
--field="$M_LEFT_KEY:LBL" "" \
--field="$M_CUSTOMIZE:LBL" "" \
--field="$OWN_COMMANDS_TOP:FBTN" "xdg-open $HOME/.config/mabox/places-prepend.csv" \
--field="$OWN_COMMANDS_BOTTOM:FBTN" "xdg-open $HOME/.config/mabox/places-append.csv" \
--field=" :LBL" "" \
--field="$M_MORE:FBTN" "jgmenusettings-pipe -s" \
--field=" $M_COLORSETTINGS!colorizer!:FBTN" "colorizer-menus -s" \
--field="$M_MAIN:FBTN" "mb-jgtools main" \
--field="$M_MAIN_KEY:LBL" "" \
--field=" :LBL" "" \
--field="$M_EDIT_MAIN:FBTN" "xdg-open $HOME/.config/mabox/favorites.csv" \
--field="$M_EDIT_MAIN_AFTER:FBTN" "xdg-open $HOME/.config/mabox/mainmenu_below_apps.csv" \
--field=" :LBL" "" \
--field=" :LBL" "" \
--field=" :LBL" "" \
--field="$SETTINGS_MENU:FBTN" "mb-jgtools settings" \
--field="<small>(super+s)</small>:LBL" "" \
--field=" :LBL" "" \
--field="$SETTINGS_EDIT:FBTN" "xdg-open $HOME/.config/mabox/settings.csv" \
--field=" :LBL" "" \
--field=" :LBL" "" \
--field=" :LBL" "" \
--field=" :LBL" "" \
--field="$M_RIGHT:FBTN" "mb-jgtools right" \
--field="$M_RIGHT_KEY:LBL" "" \
--field=" :LBL" "" \
--field="$OWN_COMMANDS_TOP:FBTN" "xdg-open $HOME/.config/mabox/right-prepend.csv" \
--field="$OWN_COMMANDS_BOTTOM:FBTN" "xdg-open $HOME/.config/mabox/right-append.csv" \
--field=" :LBL" "" \
--field=" :LBL" "" \
> $res3 &
#--field="$M_LEFT_DESC:LBL" "" \
#--field="$M_RIGHT_DESC:LBL" "" \

# WALLPAPERS
GDK_BACKEND=x11 yad --plug=$KEY --tabnum=4 --borders=20 \
--text="$WALLPAPER_DESC \n" --image="$HOME/.config/mabox/wpicon.png" \
--columns=2 --form \
--field="INFO:FBTN" "notify-send.sh -t 12000 --icon=$HOME/.config/mabox/wpicon.png '$WP_TITLE' '$WP_MSG' " \
--field="$WP_RANDOM:FBTN" "mbwallpaper -o" \
--field="$WP_CHOOSE:FBTN" "pcmanwp" \
--field="$WP_PREVIEW:FBTN" "mbwallpaper -c" \
--field=" :LBL" "" \
--field="$WP_SLIDE:FBTN" "run_wallpaperslideshow" \
--field="$WP_GENERATE:FBTN" "jgwallpapergenerate -s" \
--field="$WP_DIRS:FBTN" "xdg-open $HOME/.config/mabox/wallp_dirs.conf" \
> $res4 &

# CONKY
GDK_BACKEND=x11 yad --plug=$KEY --tabnum=5 --borders=20 \
--text="$CONKY_DESC \n" --image=conky \
--form \
--field=" Conky Manager!colorizer!:FBTN" "colorizer-conky -s" \
--field="$ADVANCED :LBL" "" \
--field="$OPEN_CONKYDIR:FBTN" "exo-open $HOME/.config/conky/" \
> $res5 &

# FONTS
GDK_BACKEND=x11 yad --plug=$KEY --tabnum=6 --borders=20 \
--text="$FONT_DESC \n" --image=font \
--form \
--field="$FONT_MENU!colorizer!:FBTN" "colorizer-fonts -s" \
--field=" :LBL" "" \
--field="$FONT_INC :FBTN" "fontctl inc_all" \
--field="$FONT_DEC :FBTN" "fontctl dec_all" \
--field="$FONT_RESET :FBTN" "fontctl resetall" \
> $res6 &

# Picom 
GDK_BACKEND=x11 yad --plug=$KEY --tabnum=7 --borders=20 \
--text="$COMP_DESC" --image=compton \
--columns=1 --form \
--field="$COMP_EDIT:FBTN" "jgpicom-pipe -s" \
--field="$COMP_RESTART:FBTN" "mabox-compositor --restart" \
--field="$COMP_TOGGLE:FBTN" "compton_toggle" \
--field=" :LBL" "" \
--field="$COMP_DIR:FBTN" "exo-open $HOME/.config/picom/configs/" \
> $res7 &
#--field="$COMP_REMOVE:FBTN" "rm -f $HOME/.config/picom.conf" \
#--field="$COMP_DEFAULT:FBTN" "cp /etc/skel/.config/picom.conf $HOME/.config/" \

# COLORIZER
GDK_BACKEND=x11 yad --plug=$KEY --tabnum=8 --borders=20 \
--text="$COLORIZER_DESC" --image=colorizer \
--form \
--columns=2 \
--field=" Colorizer!colorizer!:FBTN" "ycolorizer" \
--field="$COL_MODULES :LBL" "" \
--field="OpenBox:FBTN" "colorizer-ob -s" \
--field="Conky Manager:FBTN" "colorizer-conky -s" \
--field="Menu/SidePanels:FBTN" "colorizer-menus -s" \
--field="Fonts:FBTN" "colorizer-fonts -s" \
--field="Picom:FBTN" "jgpicom-pipe -s" \
--field=" :LBL" "" \
--field="$COL_MENU:FBTN" "colorizer -s" \
--field=" :LBL" "" \
--field=" :LBL" "" \
--field="$COL_ADDONS :LBL" "" \
--field="PyRadio:FBTN" "colorizer-pyradio -s" \
--field="Cava:FBTN" "colorizer-cava -s" \
--field=" :LBL" "" \
> $res8 &

# AUTOSTART
GDK_BACKEND=x11 yad --plug=$KEY --tabnum=9 --borders=20 --uri-handler=xdg-open \
--text="<b>$AUTOSTART_HEAD</b>" --image=gtk-execute \
--columns=1 --form \
--field="$AUTOSTART_DESC:LBL" "" \
--field="$EDIT_XDG:FBTN" "yautostart" \
--field="$AUTOSTART_DESC2 :LBL" "" \
--field="$EDIT_SCRIPT <i>~/.config/openbox/autostart</i>:FBTN" "xdg-open $HOME/.config/openbox/autostart" \
--field="$AUTOSTART_RESET:FBTN" "cp /etc/skel/.config/openbox/autostart $HOME/.config/openbox/" \
> $res9 &

# SYSTEM_SPRZET
GDK_BACKEND=x11 yad --plug=$KEY --tabnum=10 --borders=0 --text="$SYSTEM_DESC" --columns=2 --align="center" --form --scroll \
--field="<b>$SETTINGS</b>:LBL" " " \
--field="$LOCALE_SETTINGS:FBTN" "manjaro-settings-manager -m msm_locale" \
--field="$LANGUAGE_PACKAGES:FBTN" "manjaro-settings-manager -m msm_language_packages" \
--field="$KERNEL:FBTN" "manjaro-settings-manager -m msm_kernel" \
--field="$USER_ACCOUNTS:FBTN" "manjaro-settings-manager -m msm_users" \
--field="$TIME_DATE:FBTN" "manjaro-settings-manager -m msm_timedate" \
--field="$KEYBOARD:FBTN" "lxinput" \
--field="$HARDWARE:FBTN" "manjaro-settings-manager -m msm_mhwd" \
--field="$MONITORS:LBL" "" \
--field="ARandr:FBTN" "arandr" \
--field="LXRandR:FBTN" "lxrandr" \
--field=" :LBL" "" \
--field="<b>Info</b>:LBL" "" \
--field=" inxi -CGAD!utilities-terminal!:FBTN" "terminator -T 'inxi -CAGD system info' -e 'inxi -CGAD;bash'" \
--field=" $FETCH!utilities-terminal!:FBTN" "terminator -e '$FETCH;bash'" \
--field=" btop!utilities-terminal!:FBTN" "terminator -T 'btop' -x $BTOP" \
> $res10 &

# WYGLĄD
GDK_BACKEND=x11 yad --plug=$KEY --tabnum=11 --borders=20 --text="$LOOK_DESC" --icons --read-dir=/usr/share/mcc/appearance --item-width=80 \
> $res11 &
# MOTYWY
GDK_BACKEND=x11 yad --plug=$KEY --tabnum=12 --borders=20 --uri-handler=xdg-open \
--text="<b>$MT_MNGR</b>\n $MT_MNGR_DESC" \
--form --field="$MT_MNGR!preferences-desktop-theme:FBTN" "mb-obthemes" \
> $res12 &

# HELP
GDK_BACKEND=x11 yad --plug=$KEY --tabnum=13 \
--image=/usr/share/icons/hicolor/128x128/apps/distributor-logo-mabox-trans.png --text-justify=center --uri-handler=xdg-open --text="$HELP_TXT" \
--form --columns="4" \
--field="www:FBTN" "xdg-open https://maboxlinux.org" \
--field="forum:FBTN" "xdg-open https://forum.maboxlinux.org" \
--field="manual:FBTN" "xdg-open https://manual.maboxlinux.org/en/" \
--field="donate:FBTN" "xdg-open https://ko-fi.com/maboxlinux" \
> $res13 &


#main window
GDK_BACKEND=x11 yad --window-icon=mcc \
    --notebook --tab-pos="left" --key=$KEY \
    --tab="$START" \
    --tab="$TINT2"\
    --tab="$MENU" \
    --tab="$WALLPAPER" \
    --tab="$CONKY" \
    --tab="$FONTS" \
    --tab="$COMPOSITOR" \
    --tab="Colorizer" \
    --tab="$AUTOSTART" \
    --tab="$SYSTEM" \
    --tab="<i>$LOOK</i>" \
    --tab="<i>$THEMES</i>" \
    --tab="$HELP"\
    --title="$TITLE" --image=/usr/share/icons/hicolor/48x48/apps/mcc.png \
    --width="720" --height="420" --image-on-top --text-justify=right --text="$MCC"  --no-buttons > $out &
}

maindialog
