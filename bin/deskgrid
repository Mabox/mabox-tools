#!/bin/bash
### deskgrid - click on the window and select area to place it on the grid
#   Works with active and inactive windows
#   (C) Daniel Napora <napcok@gmail.com>, 2021-24
#   https://maboxlinux.org
# 


CONFIG_FILE="$HOME/.config/superclick.cfg"
if [ ! -f $CONFIG_FILE ]; then
cat <<EOF > ${CONFIG_FILE}
# Gap between windows in pixels (reasonable values: 0 8 16 24)
gap=16
# Outer gap (disable if you use WM margins)
show_outer_gap=true
# Only for clicksnap (mouse) action
activate_window=false
EOF
fi
source <(grep = $CONFIG_FILE)

GAP=${gap:-16}
COLUMNS=${columns:-12}
ROWS=${rows:-12}
#TITLEBAR_HEIGHT=${titlebar_height:-18}

case "$LANG" in
    pl*)
    TITLE1="DesktopGrid"
    TEXT1="\n\nNarysuj myszą prostokąt na pulpicie aby ustalić nową pozycję i rozmiar dla okna.\nPodczas rysowania możesz wcisnąć Spację aby przesuwać zaznaczenie.\n"
    DRAWGRID="Pokaż obrazek pomocniczy jako tło"
    DISABLE_NOTIFICATIONS="Wyłącz podpowiedzi"
    ;;
    *)
    TITLE1="DesktopGrid"
    TEXT1="\nDraw rectangle by mouse to set new window positon and size.\nWhile drawing you may hold Space key to move selection.\nNew window will be bigger than selection."
    DRAWGRID="Show helper image as background"
    DISABLE_NOTIFICATIONS="Disable this hint"
    ;;
esac

eval $(xdotool getmouselocation --shell)
# we have window id now as $WINDOW
HEX_ID=$(printf '0x%x\n' $WINDOW)
CHILD_ID=$(xwininfo -id $HEX_ID -children|grep "\"" | awk '{print $1}')
if xwininfo -id $CHILD_ID -wm |grep Dock ; then exit 0 ;fi # Ignore Dock eg. tint2

CHILD=$(printf %i $CHILD_ID)
read BORDER_L BORDER_R BORDER_T BORDER_B <<< "$(xprop -id $CHILD _NET_FRAME_EXTENTS | awk ' {gsub(/,/,"");print $3,$4,$5,$6}')"
    BORDERCOMP_X=$((BORDER_L+BORDER_R))
    BORDERCOMP_Y=$((BORDER_T+BORDER_B))


xdotool windowminimize --sync $WINDOW
# Screen dimensions, margins and available size
SCREENSIZE=$(wmctrl -d |grep "*" | awk -F' ' '{print $4}')
MARGINS=$(wmctrl -d |grep "*" | awk -F' ' '{print $8}')
AVAILSIZE=$(wmctrl -d |grep "*" | awk -F' ' '{print $9}')

SCREEN_X="${SCREENSIZE%x*}"
SCREEN_Y="${SCREENSIZE#*x}"
AVAIL_X="${AVAILSIZE%x*}"
AVAIL_Y="${AVAILSIZE#*x}"
MARGIN_X="${MARGINS%,*}"
MARGIN_Y="${MARGINS#*,}"

#Show notify
if [ $notifications = true ]; then
notify-send.sh -t 20000 --icon=mbcc "$TITLE1" "$TEXT1" -o "$DRAWGRID:drawgrid" -o "$DISABLE_NOTIFICATIONS:mb-setvar notifications=false $CONFIG_FILE"
fi

# Take selection
slop=$(slop --highlight -b 3 --tolerance=0 --color=0.3,0.4,0.6,0.4 -f "%x %y %w %h") || exit 1
read -r X Y W H < <(echo $slop)

start_x="$(((X-MARGIN_X)/(AVAIL_X/COLUMNS)))"
start_y="$(((Y-MARGIN_Y)/(AVAIL_Y/ROWS)))"
end_x="$(((X-MARGIN_X+W)/(AVAIL_X/COLUMNS)))"
end_y="$(((Y-MARGIN_Y+H)/(AVAIL_Y/ROWS)))"

### Outer gap factor
if [[ $show_outer_gap = true ]]; then GF="1" ; else GF="0" ; fi

### SIZE and POS calculation
if [[ $start_x = "0" ]]; then GAP_X="$((GAP*GF))" ; else GAP_X=$((GAP/2)) ; fi
if [[ $start_y = "0" ]]; then GAP_Y="$((GAP*GF))" ; else GAP_Y=$((GAP/2)) ; fi

if [[ $end_x = $((COLUMNS-1)) ]]; then GAP_X_END="$((GAP/2))" ; else GAP_X_END=$((GAP/2)) ; fi
if [[ $end_y = $((ROWS-1)) ]]; then GAP_Y_END="$((GAP/2))" ; else GAP_Y_END=$((GAP/2)) ; fi

SIZE="$(((end_x-start_x+1)*(AVAIL_X/COLUMNS)-GAP_X/2-GAP_X_END-BORDERCOMP_X)) $(((end_y-start_y+1)*(AVAIL_Y/ROWS)-GAP_Y/2-GAP_Y_END-BORDERCOMP_Y))"

POSITION="$((start_x*AVAIL_X/COLUMNS+GAP_X/2+MARGIN_X)) $((start_y*AVAIL_Y/ROWS+GAP_Y/2+MARGIN_Y))"

xdotool windowsize $WINDOW $SIZE
xdotool windowmove $WINDOW $POSITION
xdotool windowmap $WINDOW

