#!/bin/bash
#
#   Mabox Tint2 Editor
#
#   Written by damo <damo@bunsenlabs.org> for BunsenLabs Linux, April 2015
#   Ported to Manjaro by napcok <napcok@gmail.com>
#
########################################################################
#
#   Tint2 files must be in $TINT2PATH
#
#   Checkmarked tint2s will be opened in the text editor
#   Multiple tint2s can be chosen
#
########################################################################

TINT2PATH="$HOME/.config/tint2"

### DIALOG VARIABLES
DLGDEC="yad --center  --borders=20 --width=400 --height=600 " 
WINICON="--window-icon=distributor-logo-mabox --image=tint2conf"
case $LANG in
    pl*)
    TITLE="Edytor Tint2"
    OK="--button=OK:0"
    CANCEL="--button=Anuluj:1"
    TE_DESC="Wybierz pliki konfiguracji Tint2 do edycji.\nMożesz wybrać kilka.\nDodatkowe pliki konfiguracyjne można skopiować do katalogu: \n<i>$TINT2PATH</i>"
    TE_CHOOSE="Wybór"
    TE_CONFFILE="Plik konfiguracyjny"
    ;;
    *)
    TITLE="Tint2 Editor"
    OK="--button=OK:0"
    CANCEL="--button=Cancel:1"
    TE_DESC="Choose Tint2rc file for edit.\nYou can choose multiple files.\nYou can put your own Tint2 configurations\ninto <i>$TINT2PATH</i> directory."
    TE_CHOOSE="Choose"
    TE_CONFFILE="Configuration file"
    ;;
esac

########## FUNCTIONS ###################################################

fillArrays(){
    num="$1"
    tintsPath[$num]="$2"   # full filepath to tint2
    tintsArr[$num]="$3"    # displayed name: "directory/tintfile" 
}

findTint(){
# search dirs for tint2 config files
    num=0
    for x in $(find $TINT2PATH -type f );do
        # check if likely tint2 config file
        pm=$(grep  "panel_monitor" "$x")
        if [[ $pm ]];then
            TINT2=$( echo $x | awk -F"/" '{print $(NF-1)"/"$NF}')
            fillArrays $num $x $TINT2
            num=$(($num+1))
        fi
    done
}
######## END FUNCTIONS #################################################

# get tint2 directories, add to array
findTint

# loop through arrays, and build msg text for yad dialog
unset LISTINT
for ((j=0; j<${#tintsArr[*]}; j++));do
    LISTINT="$LISTINT FALSE ${tintsArr[j]} "
done
## Populate dialog from array, get return value(s)
RET=$($DLGDEC $WINICON --list --title="$TITLE" \
    --text="$TE_DESC" \
    --checklist \
    --column="$TE_CHOOSE:CHK" --column="$TE_CONFFILE:TXT" $LISTINT --separator=":" \
    $CANCEL $OK \
    )

if (( $? == 1 )); then # cancel button pressed
    exit 0
else
    i=0
    OIFS=$IFS   # save Internal Field Separator
    IFS=":"     # separator is ":" in returned choices
    for name in $RET; do 
        retTint[$i]="$name"
        i=$(($i+1))
    done
    IFS=$OIFS   # reset IFS back to default
    
    # Find the chosen tint2s and edit them
    for name in ${retTint[*]};do       # loop through checkmarked tint2 names
        for ((j=0; j<${#tintsPath[*]}; j++));do  # traverse through elements
            for f in ${tintsPath[j]};do    # compare with choice from dialog
                display=$( echo "$f" | awk -F"/" '{print $(NF-1)"/"$NF}')
                if [[ $display = $name ]];then
                    xdg-open "$f"
                fi
            done
        done
    done 
fi

exit 0
